//
//  UIView_Extention.swift
//  FyusionSampleProject
//
//  Created by Farzad on 5/12/20.
//  Copyright © 2020 Farzad. All rights reserved.
//

import Foundation
import UIKit
var imageCache = NSCache<NSURL, UIImage>()
extension UIImageView {
    func loadImageFrom(resource:Resource<UIImage>) {
        let nsURL = resource.url as NSURL
        self.image = #imageLiteral(resourceName: "default_image")
        if let imageFromCache = imageCache.object(forKey: nsURL) {
            self.image = imageFromCache
            return
        }
        FyuseWebApi.shared.fetch(resource: resource) { result in
                switch result {
                case .success(let image):
                    imageCache.setObject(image, forKey: nsURL)
                    self.image = image
                case .failure(let error):
                    print(error)
                }
        }
    }
}
