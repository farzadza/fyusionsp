//
//  model.swift
//  FyusionSampleProject
//
//  Created by Farzad on 5/11/20.
//  Copyright © 2020 Farzad. All rights reserved.
//

import Foundation
import UIKit


struct FyuseDataModel {
    let uid:String
    let path:String
    let thumb:String
    let category:[String]
    let description:String?
    enum CodingKeys: String {
        case dataContainer = "data"
        case listContainer = "list"
        case uid = "uid"
        case path = "path"
        case thumb = "thumb"
        case category = "category"
        case description = "description"
    }
    
}



extension FyuseDataModel {
    init?(dict:[String:AnyObject]) {
        if let uid = dict[CodingKeys.uid.rawValue] as? String,
            let path = dict[CodingKeys.path.rawValue] as? String,
            let thumbUrl = dict[CodingKeys.thumb.rawValue] as? String,
            let category = dict[CodingKeys.category.rawValue] as? [String] {
            let description = dict[CodingKeys.description.rawValue] as? String
            self.category = category
            self.uid = uid
            self.description = description
            self.path = path
            self.thumb = thumbUrl
        } else {
            return nil
        }
    }
    
    
    static let all = Resource<[FyuseDataModel]>(url:URL(string: "https://api.fyu.se/1.4/group/web/jlp75qdubezgc?abspath=1")!) { data in
        
        let container = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
        var list = [FyuseDataModel]()
        if let fyuseDataList = container?["data"]?["list"] as? [[String:AnyObject]]{
            fyuseDataList.forEach { dict in
                if let newItem = FyuseDataModel(dict: dict) {
                    list.append(newItem)
                }
            }
        }
        return list
    }
    var thumbeImage:Resource<UIImage> {
        let thumbImageResource = Resource<UIImage>(url:URL(string: self.thumb)!) { data in
            return UIImage(data: data)
        }
        return thumbImageResource
    }
    var pathImage:Resource<UIImage> {
        let patchImageResource = Resource<UIImage>(url:URL(string: self.path)!) { data in
              return UIImage(data: data)
          }
          return patchImageResource
      }
}

