//
//  ViewController.swift
//  FyusionSampleProject
//
//  Created by Farzad on 5/11/20.
//  Copyright © 2020 Farzad. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
       
             FyuseWebApi.shared.fetch(resource: FyuseDataModel.all) { [weak self] (result) in
                 switch result {
                 case .success(let list):
                      let tableVC = TableViewController(dataSource: list) as TableViewController
                      tableVC.modalPresentationStyle = .fullScreen
                      self?.present(tableVC, animated: true, completion: nil)
                 case .failure(let error):
                     print(error)
                 }
                self?.spinner.stopAnimating()
             }
  
    }
   

}

