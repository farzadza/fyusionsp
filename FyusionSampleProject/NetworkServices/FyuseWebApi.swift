//
//  Endpoint.swift
//  FyusionSampleProject
//
//  Created by Farzad on 5/11/20.
//  Copyright © 2020 Farzad. All rights reserved.
//

import Foundation
import UIKit

struct Resource<T> {
    let url:URL
    let parse:(Data) -> T?
}

class FyuseWebApi {
    
    typealias JSONDictionary = [String: Any]
    public enum APIServiceError: Error {
        case apiError
        case invalidEndpoint
        case invalidResponse
        case noData
        case decodeError
    }
    public static let shared = FyuseWebApi()
    private init() {}
    private let urlSession = URLSession.shared
    
    /// Returns `true` if `code` is in the 200..<300 range.
    func expected200to300(_ code: Int) -> Bool {
        return code >= 200 && code < 300
       
    }

  
    func fetch<T>(resource: Resource<T>,onComplete:@escaping (Result<T,APIServiceError>) -> ()) {
        urlSession.dataTask(with: resource.url) { result  in
             DispatchQueue.main.async {
            switch result {
            case .success(let (response, data)):
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode,statusCode >= 200 && statusCode < 300  else {
                    onComplete(.failure(.invalidResponse))
                    return
                }
                if let parsedData = resource.parse(data) {
                    onComplete(.success(parsedData))
                } else {
                    onComplete(.failure(.decodeError))
                }
            case .failure(let error):
                onComplete(.failure(.apiError))
                } }
        }.resume()
    }
}


extension URLSession {
    func dataTask(with url: URL, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionDataTask {
        return dataTask(with: url) { (data, response, error) in
            if let error = error {
                result(.failure(error))
                return
            }
            guard let response = response, let data = data else {
                let error = NSError(domain: "error", code: 0, userInfo: nil)
                result(.failure(error))
                return
            }
            result(.success((response, data)))
        }
    }
}

