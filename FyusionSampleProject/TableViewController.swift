//
//  TableViewController.swift
//  FyusionSampleProject
//
//  Created by Farzad on 5/12/20.
//  Copyright © 2020 Farzad. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var dataSource = [FyuseDataModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
      tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
     }
    convenience init(dataSource:[FyuseDataModel]) {
        self.init()
        self.dataSource = dataSource
        tableView.reloadData()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
  
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        configureImageView(imageView: cell.imageView!)
        cell.imageView?.loadImageFrom(resource: dataSource[indexPath.row].thumbeImage)
        configureImageView(imageView: cell.imageView!)
        cell.textLabel?.text = dataSource[indexPath.row].uid
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        detailVC.dataSource = self.dataSource[indexPath.row]
        let navigationController = UINavigationController(rootViewController: detailVC)
        detailVC.navigationItem.title = self.dataSource[indexPath.row].uid
        navigationItem.hidesBackButton = false
        detailVC.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    private func configureImageView(imageView:UIImageView) {
        imageView.layer.borderWidth = 3
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        
    }
}
