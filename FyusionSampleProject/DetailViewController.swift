//
//  DetailViewController.swift
//  FyusionSampleProject
//
//  Created by Farzad on 5/13/20.
//  Copyright © 2020 Farzad. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var detailImageView: UIImageView!
    var dataSource:FyuseDataModel?
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let dataSource = self.dataSource else {return}
        
        configure(with: dataSource)
    }
    
    private func getAllCategories(categoryList:[String]) -> String {
        var categories = ""
        categoryList.forEach { category in
            categories = "categories, \(category)"
        }
        return categories
    }
    
    private func configure(with data:FyuseDataModel) {
        spinner.startAnimating()
        detailImageView.image = #imageLiteral(resourceName: "default_image")
        detailImageView.layer.cornerRadius = 10
        detailImageView.clipsToBounds = false
        detailImageView.alpha = 1.0
        detailImageView.loadImageFrom(resource: data.pathImage )
        spinner.stopAnimating()
        descriptionLabel.text = data.description
        categoryLabel.text = getAllCategories(categoryList: data.category)
    }
    
    
    
}
